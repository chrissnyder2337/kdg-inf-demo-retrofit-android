package be.kdgdemo.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;

import be.kdgdemo.service.KdgService;
import be.kdgdemo.visualisation.Visualisation;
import be.kdgdemo.visualisation.VisualisationForCrouton;
import butterknife.ButterKnife;

import static be.kdgdemo.application.KdgDemoApplication.getKdgService;

/**
 * @author Jo Somers
 */
public class BaseActivity extends Activity {

    private Visualisation visualisation;

    private KdgService kdgService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        kdgService = getKdgService();
        visualisation = new VisualisationForCrouton(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        visualisation.cancelForActivity();
        ButterKnife.reset(this);
    }

    public KdgService getService() {
        return kdgService;
    }

    public Visualisation getVisualisation() {
        return visualisation;
    }
}
