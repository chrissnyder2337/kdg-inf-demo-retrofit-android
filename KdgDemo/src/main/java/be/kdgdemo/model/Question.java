package be.kdgdemo.model;

import org.joda.time.DateTime;

import java.util.List;

/**
 * @author Jo Somers
 */
public class Question {

    private String id;

    private String question;

    private DateTime modified;

    private List<Answer> answers;

    /**
     * Mandatory empty constructor
     */
    public Question() {
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public DateTime getModified() {
        return modified;
    }

    public String getId() {
        return id;
    }

}
