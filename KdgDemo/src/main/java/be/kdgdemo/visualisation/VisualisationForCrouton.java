package be.kdgdemo.visualisation;

import android.app.Activity;

import de.keyboardsurfer.android.widget.crouton.Crouton;

import static de.keyboardsurfer.android.widget.crouton.Crouton.clearCroutonsForActivity;
import static de.keyboardsurfer.android.widget.crouton.Style.ALERT;

/**
 * @author Jo Somers
 */
public class VisualisationForCrouton implements Visualisation {

    private final Activity activity;

    public VisualisationForCrouton(final Activity activity) {
        this.activity = activity;
    }

    /**
     * Show error visualisation
     *
     * @param message {@link String}
     */
    @Override
    public void showError(String message) {
        Crouton
                .makeText(activity, message, ALERT)
                .show();
    }

    /**
     * Cancel all visualisations for the given {@link android.app.Activity}
     */
    @Override
    public void cancelForActivity() {
        clearCroutonsForActivity(activity);
    }

}
